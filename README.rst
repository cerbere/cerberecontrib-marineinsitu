===========================
cerberecontrib-marineinsitu
===========================

This package is complementary to Cerbere package.

It provides reader classes for various marine in situ datasets, including:

  * CMEMSINSTAC : in situ files from Copericus Marine Service In Situ TAC


.. code-block:: python

  # example : opening a CMEMS In Situ TAC product file
  import cerbere

  dst = cerbere.open_dataset(
      'tests/data/CMEMSINSTAC/GL_TS_DB_4402730_20231122.nc',
      'CMEMSINSTAC',
  )

  # this should work too, as Cerbere tries to guess the reader from filename
  # pattern
  dst = cerbere.open_dataset(
        'tests/data/CMEMSINSTAC/GL_TS_DB_4402730_20231122.nc')


Using features
==============

  # example : opening a CMEMS In Situ TAC product file as a feature
  import cerbere

  feature = cerbere.open_feature(
      'tests/data/CMEMSINSTAC/GL_TS_DB_4402730_20231122.nc')


Features in CMEMS In Situ TAC datasets
======================================

Using cerbere feature classes with CMEMS In Situ TAC data depends on the type
and structure of the different file categories provided.

The matching between
these categories and their corresponding cerbere feature class is provided in
the table below:

================== =================
CMEMS File pattern feature class
================== =================
GL_TS_MO*	       TrajectoryProfile
GL_TS_DB*	       Trajectory
GL_PR_PF_*	       TrajectoryProfile
GL_WS_MO_*	       Trajectory
================== =================

This association may seem counter-intuitive in some cases: for instance mooring
profiles (``GL_TS_MO*``) should be associated with cerbere ``TimeSeriesProfile``
feature type: however, due to the CMEMS format structure for these data with
possibly varying lat/lon (instead of a fixed station with invariant lat/lon)
this was not possible.

.. code-block:: python

