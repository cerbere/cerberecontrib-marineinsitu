import logging
import warnings
from pathlib import Path
import re
import typing as T

import numpy as np
import xarray as xr
import dask

import cerbere
import cerbere.cfconvention as cf
from cerbere.reader.basereader import BaseReader

# Matching between CDM data type and CF featureType
FEATURE_TYPES = {
    # 'PF': 'profile',
    # 'DB': 'profile',
    # 'Argo profile': 'profile',
    # 'vertical profile': 'profile',
    'profile': 'profile',
    # 'Time-series': 'timeSeriesProfile',
    # 'Time-series profile': 'timeSeriesProfile',
    'timeseries': 'timeSeriesProfile',
    'trajectory': 'trajectory',
    'station': 'timeSeries',
    'station_profile': 'profileTimeSeries',
    'trajectory_profile': 'trajectoryProfile',
}


FEATURE_AXIS = {
    'profile': 'profile',
    'vertical profile': 'profile',
    'timeseries': 'profile',
    'time-series': 'profile',
    'time-series profile': 'profile',
    'station': 'time',
    'trajectory': 'time',
    'trajectory_profile': 'profile',
    'station_profile': 'profile'
}


CF_ROLE = {
    'trajectory': 'trajectory_id',
    'trajectoryProfile': 'trajectory_id',
    'timeSeriesProfile': 'timeseries_id',
    'timeSeries': 'timeseries_id',
}


class CMEMSINSTAC(BaseReader):
    """
    CMEMS INS TAC data exist with different patterns:
    * CO_DMQCGL01_20201228_PR_ML.nc in MultiYear products
    * GL_WS_MO_41010_20201006 in NRT products
    """

    pattern: str = re.compile(
        r"^[A-Z]{2}_[A-Z]{2}_[A-Z]{2}_.*nc$|" \
        r"^CO_DMQCGL01_.*[A-Z]{2}_[A-Z]{2}.nc$|" \
        r"^D[0-9]{7}_[0-9]{3}.nc$"
        )
    engine: str = "netcdf4"
    description: str = "Use CMEMS In Situ TAC files in Xarray"
    url: str = "https://link_to/your_backend/documentation"


    @staticmethod
    def _guess_and_fix_z_axis(ds):
        """fix the Z axis

        CMEMSINSTAC provides (wrongly) several Z axes, there should be only one.
        The function guesses which one should be the main Z axis and remove
        the axis attribute of the other variables.
        """
        valid_z_coords = ['PRES', 'DEPH']

        # collect variables with axis attribute Z. Ignore variables other than
        # the valid one (depth or pressure)
        z_coords = []
        for v in ds.variables:
            if ds[v].attrs.get('axis') == 'Z':
                if v in valid_z_coords:
                    z_coords.append(v)
                else:
                    ds[v].attrs.pop('axis')

        # if none, try to guess which variable to consider for Z axis
        if len(z_coords) == 0:
            for v in valid_z_coords:
                if v in ds.variables:
                    z_coords.append(v)
                    ds[v].attrs['axis'] = 'Z'

        if len(z_coords) > 1:
            for v in z_coords:
                # remove wrong Z axis attributes
                if v not in ['PRES', 'DEPH']:
                    ds[v].attrs.pop('axis')

            # chose between DEPH and PRES, by order of priority
            if 'DEPH' in z_coords and ds['DEPH'].count() > 0:
                ds['PRES'].attrs.pop('axis')
            elif 'PRES' in z_coords and ds['PRES'].count() > 0:
                ds['DEPH'].attrs.pop('axis')
            if ds['DEPH'].count() == ds['PRES'].count() == 0:
                warnings.warn(
                    f'Neither PRES or DEPH have valid values. DEPH kept as '
                    f'default Z axis',
                    cerbere.CWarning
                )
            if ds['DEPH'].count() > 0 and ds['PRES'].count() > 0:
                warnings.warn(
                    f'Both PRES or DEPH have valid values. Can not decide '
                    f'which one should be Z axis. DEPH kept as default Z axis',
                    cerbere.CWarning
                )

        return ds

    @staticmethod
    def _guess_and_fix_feature_type(ds):
        """Guess the feature type from the metadata"""
        # fix wrong CDM data type spellings
        if 'cdm_data_type' in ds.attrs:
            ds.attrs['cdm_data_type'] = dict(
                timeSeries='station',
                trajectoryProfile='trajectory_profile'
            ).get(ds.attrs['cdm_data_type'], ds.attrs['cdm_data_type'])
            cdm = ds.attrs['cdm_data_type']
            if cdm not in FEATURE_TYPES.keys():
                raise cerbere.FeatureTypeError(
                    f'Unknown feature type {cdm}')

        # fix wrong featureType data type spellings
        if 'featureType' in ds.attrs:
            ds.attrs['featureType'] = dict(
                timeseries='timeSeries',
                timeseriesProfile='timeSeriesProfile',
            ).get(ds.attrs['featureType'], ds.attrs['featureType'])

        if 'cdm_data_type' not in ds.attrs and 'featureType' not in ds.attrs:

            # guess from DATA_TYPE variable
            if 'DATA_TYPE' not in ds:
                raise cerbere.FeatureTypeError(
                    f'no variable or attribute to get the feature type')

            data_type = str(ds['DATA_TYPE'].astype(str).data).strip()
            if data_type not in FEATURE_TYPES:
                raise cerbere.FeatureTypeError(
                    f'Unknown data type {data_type} in DATA_TYPE variable. '
                    f'Update the reader FEATURE_TYPES dict to add this new '
                    f'type or the feature will not be properly recognised')

            cdm_data_type = FEATURE_TYPES[data_type]

        else:
            cdm_data_type = ds.attrs['cdm_data_type']

        # fix wrong CDM feature type
        if cdm_data_type == 'trajectory' and 'DEPTH' in ds.dims:
            if 'DEPH' in ds.coords and ds.sizes['DEPTH'] > 1:
                # fix feature type (trajectory => trajectoryProfile
                ds.attrs['cdm_data_type'] = 'trajectory_profile'
                ds.attrs['featureType'] = 'trajectoryProfile'
                cdm_data_type = ds.attrs['cdm_data_type']
            if ds.sizes['DEPTH'] == 1:
                # remove dummy dimensions
                ds = ds.squeeze('DEPTH', drop=False)

        if cdm_data_type == 'station':
            if 'DEPH' in ds.coords:
                z = 'DEPH'
            elif 'PRES' in ds.coords:
                z = 'PRES'
            else:
                raise cerbere.CoordinateError('No Z coordinate found')

            z_qc = z + '_QC'
            if z_qc in ds and ds[z].dims != ds[z_qc].dims:
                ds.attrs['cdm_data_type'] = 'station_profile'
                cdm_data_type = ds.attrs['cdm_data_type']
                if len(ds[z].dims) == 0:
                    # BO_TS_MO_EspooKyto_20240416.nc
                    ds[z] = ds[z].expand_dims(ds[z_qc].sizes)

                if 'DEPTH' not in ds.dims:
                    ds[z] = ds[z].expand_dims(DEPTH=1)
                    ds[z_qc] = ds[z_qc].expand_dims(DEPTH=1)

            elif ds.sizes['DEPTH'] > 1:
                # ex: GL_TS_MO_46100_20231217
                ds.attrs['cdm_data_type'] = 'station_profile'
                cdm_data_type = ds.attrs['cdm_data_type']
            elif ds.sizes['DEPTH'] == 1:
                if len(ds[z].dims) > 1:
                    # varying depth => profile
                    # ex: GL_TS_MO_SCLD1_20240315
                    ds.attrs['cdm_data_type'] = 'station_profile'
                    cdm_data_type = ds.attrs['cdm_data_type']
                else:
                    # ex: GL_TS_MO_EOL_20240315.nc
                    ds = ds.squeeze('DEPTH', drop=False)

        if cdm_data_type == 'profile':
            if 'DEPTH' not in ds.dims:
                # ex: GL_PR_PF_5906104_20231217.nc
                ds.attrs['cdm_data_type'] = 'trajectory'
            else:
                # ex: GL_PR_BO_WXAQ.nc
                ds.attrs['cdm_data_type'] = 'trajectory_profile'
            cdm_data_type = ds.attrs['cdm_data_type']

        # fix wrong feature and cdm type for moorings
        if len(ds.LATITUDE.dims) == 0 and 'DEPTH' in ds.dims:
            # ex: GL_TS_MO_41043_20231217.nc
            if ds.attrs['featureType'] == 'timeSeries':
                ds.attrs['featureType'] = 'trajectoryProfile'
                ds.attrs['cdm_data_type'] = 'station_profile'
                cdm_data_type = ds.attrs['cdm_data_type']

        # CF featureType attribute
        if 'featureType' not in ds.attrs:
            ds.attrs['featureType'] = cf.CDM2CF_FEATURE[cdm_data_type]

        if ds.attrs['featureType'] != cf.CDM2CF_FEATURE[cdm_data_type]:
            warnings.warn(
                f'Mismatch between cdm_data_type and featureType value: '
                f'{ds.attrs["featureType"]} <> {cdm_data_type}. Renaming '
                f'featureType to {cf.CDM2CF_FEATURE[cdm_data_type]}')
            ds.attrs['featureType'] = cf.CDM2CF_FEATURE[cdm_data_type]

        ds.attrs['cdm_data_type'] = cdm_data_type

        return ds

    @staticmethod
    def guess_cf_role(ds):
        """set proper cf_role

        helps discriminate for instance trajectoryProfile from IMDCollection
        which share the same feature requirements
        """
        cf_role = CF_ROLE[ds.attrs['featureType']]

        # check if there is already a CF role
        for _ in ds.variables:
            v = ds.variables[_]
            if 'cf_role' in v.attrs:
                if v.attrs['cf_role'] != cf_role:
                    raise cerbere.FeatureTypeError(
                        f'The cf_role attribute {v.attrs["cf_role"]} does not '
                        f'match the featureType {ds.attrs["featureType"]}')
                return ds

        feat_type = ds.attrs['featureType']
        if feat_type in CF_ROLE.keys():
            if 'PLATFORM_NUMBER' in ds:
                ds.PLATFORM_NUMBER.attrs['cf_role'] = CF_ROLE[feat_type]
            elif 'platform_code' in ds.attrs:
                # create variable with required cf_role
                cf_role_var = CF_ROLE[ds.attrs['featureType']].strip('_id')
                ds[cf_role_var] = ds.attrs['platform_code']
                ds[cf_role_var].attrs['cf_role'] = CF_ROLE[feat_type]

        return ds

    @staticmethod
    def postprocess(ds, decode_times: bool = True, **kwargs):

        # check and fix feature type
        ds = CMEMSINSTAC._guess_and_fix_feature_type(ds)

        # fix Z axis attributes
        ds = CMEMSINSTAC._guess_and_fix_z_axis(ds)

        # fix T axis attribute
        if 'JULD' in ds and 'TIME' not in ds:
            ds['JULD'].attrs['axis'] = 'T'

        # set proper cf_role
        ds = CMEMSINSTAC.guess_cf_role(ds)

        # rename dimensions to match feature(s instance dimension in a
        # collection
        removed_dims = {'LATITUDE', 'LONGITUDE', 'POSITION', 'TIME'}
        main_axis = FEATURE_AXIS[ds.attrs['cdm_data_type'].lower()]
        for vname in ds.variables:
            varr = ds[vname]
            bad_dims = set(varr.dims).intersection(removed_dims)
            assert (len(bad_dims) <= 1)
            if len(bad_dims) == 1:
                ds[vname] = varr.swap_dims(
                    {list(bad_dims)[0]: main_axis})

        if 'time_coverage_start' not in ds.attrs:
            # temporal coverage
            ds.attrs['time_coverage_start'] = cerbere.dt64todatetime(
                ds.cb.time.min())
            ds.attrs['time_coverage_end'] = cerbere.dt64todatetime(
                ds.cb.time.max())

        # fix dummy depths
        if 'DEPH' in ds and ds.DEPH.min() == -12000:
            ds['DEPH'][ds.DEPH==-12000] = np.nan

        return ds

    @staticmethod
    def open_dataset(*args, **kwargs):
        """Open a CMEMS INSTAC dataset.

        Fixes format issues preventing from directly opening it with xarray
        """
        try:
            return xr.open_dataset(*args, **kwargs)
        except ValueError:
            # issue with GL_WS_MO files (can't be opened with xarray)
            import netCDF4

            dst = xr.open_dataset(*args, drop_variables=['FREQUENCY'], **kwargs)
            dst = dst.rename_dims(
                {'FREQUENCY': 'freq'}
            )

            # read frequencies with netCDF4 API
            ncdst = netCDF4.Dataset(*args)

            return dst.assign_coords({
                'frequency': xr.DataArray(
                    dims=('time', 'freq',),
                    data=ncdst.variables['FREQUENCY'][:],
                    attrs=ncdst.variables['FREQUENCY'].__dict__)})
