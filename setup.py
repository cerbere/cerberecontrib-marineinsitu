# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


requires = ['cerbere']

setup(
    name='cerberecontrib-marineinsitu',
    version='2.0',
    url='',
    download_url='',
    license='GPLv3',
    author='Jeff Piolle',
    author_email='jfpiolle@ifremer.fr',
    description='Cerbere extension for various marine in situ data product',
    long_description="This package contains the Cerbere extension for various marine in situ data product.",
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GPLv3 License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Documentation',
        'Topic :: Utilities',
    ],
    entry_points={
        'cerbere.reader': [
            'CMEMSINSTAC = '
            'cerberecontrib_marineinsitu.reader.cmemsinstac:CMEMSINSTAC',
        ]
    },
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    package_data={},
    install_requires=requires,
)
