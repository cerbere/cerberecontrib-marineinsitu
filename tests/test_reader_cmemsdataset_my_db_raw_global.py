"""
Test class for cerbere CMEMSINSTAC reader with Multi-Year drifting buoy
data (considered in their format as a collection of profiles), e.g.:

INSITU_GLO_PHY_TS_DISCRETE_MY_013_001/
cmems_obs-ins_glo_phy-temp-sal_my_cora_irr/
RAW_data/
global/
CO_DMQCGL01_20200806_TS_DB.nc

This dataset contains multiple profiles over a given day from various
drifters. The data are reported as an irregular multi-dimensional array of
profiles (IMDCollection[Profile] Cerbere feature)

:copyright: Copyright 2023 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from cerbere.reader.pytest_reader import *
from cerbere.feature.cimdcollection import IMDCollection


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
        test_data_dir,
        'CMEMSINSTAC',
        'CO_DMQCGL01_20200806_TS_DB.nc'
    )


@pytest.fixture(scope='module')
def reader():
    return 'CMEMSINSTAC'


@pytest.fixture(scope='module')
def feature_class():
    return IMDCollection


def test_cdm_feature_type(feature):
    assert feature.ds.cb.cdm_data_type == 'profile'
    assert feature.ds.cb.featureType == 'profile'

    cf_role = [
        feature.ds[_].attrs['cf_role']
        for _ in feature.ds.variables if 'cf_role' in feature.ds[_].attrs]
    assert cf_role == []

