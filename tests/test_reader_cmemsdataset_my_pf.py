"""
Test class for cerbere CMEMSINSTAC reader with Multi-Year profile
data, e.g.:

INSITU_GLO_PHY_TS_DISCRETE_MY_013_001/
cmems_obs-ins_glo_phy-temp-sal_my_cora_irr/
RAW_data/
global/
test_reader_cmemsdataset_my_db_raw_global.py

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from cerbere.reader.pytest_reader import *
from cerbere.feature.cimdcollection import IMDCollection


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
        test_data_dir,
        'CMEMSINSTAC',
        'CO_DMQCGL01_20201222_PR_PF.nc'
    )


@pytest.fixture(scope='module')
def reader():
    return 'CMEMSINSTAC'


@pytest.fixture(scope='module')
def feature_class():
    return IMDCollection


