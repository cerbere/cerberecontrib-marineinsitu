"""
Test class for cerbere CMEMSINSTAC reader with NRT / History drifting buoy
data, e.g.:

INSITU_GLO_PHYBGCWAV_DISCRETE_MYNRT_013_030/
cmems_obs-ins_glo_phybgcwav_mynrt_na_irr/
history/DB/GL_TS_DB_4402609.nc

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from cerbere.reader.pytest_reader import *
from cerbere.feature.cimdcollection import IMDCollection
from cerbere.feature.ctrajectoryprofile import TrajectoryProfile


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
        test_data_dir,
        'CMEMSINSTAC',
        'GL_TS_DB_4402609.nc'
    )


@pytest.fixture(scope='module')
def reader():
    return 'CMEMSINSTAC'


@pytest.fixture(scope='module')
def feature_class():
    return TrajectoryProfile



