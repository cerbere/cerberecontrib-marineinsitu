"""
Test class for cerbere CMEMSINSTAC reader with NRT / Latest drifting buoy
data, CMEMS format v2.0, e.g.:

INSITU_GLO_PHYBGCWAV_DISCRETE_MYNRT_013_030/
cmems_obs-ins_glo_phybgcwav_mynrt_na_irr/
latest/20231122/GL_TS_DB_4402730_20231122.nc

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from cerbere.reader.pytest_reader import *
from cerbere.feature.ctrajectory import Trajectory


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
        test_data_dir,
        'CMEMSINSTAC',
        'GL_TS_DB_4402730_20231122.nc'
    )


@pytest.fixture(scope='module')
def reader():
    return 'CMEMSINSTAC'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory
