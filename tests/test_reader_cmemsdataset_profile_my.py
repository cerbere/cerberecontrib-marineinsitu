"""
Test class for cerbere CMEMSINSTAC reader with MultiYear Argo profile product

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from cerbere.reader.pytest_reader import *
from cerbere.feature.cimdcollection import IMDCollection


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
        test_data_dir,
        'CMEMSINSTAC',
        'D1900381_006.nc'
    )


@pytest.fixture(scope='module')
def reader():
    return 'CMEMSINSTAC'


@pytest.fixture(scope='module')
def feature_class():
    return IMDCollection

