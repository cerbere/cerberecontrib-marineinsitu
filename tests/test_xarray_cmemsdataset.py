"""
Test class for cerbere CMEMSInsituDataset dataset

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from tests.checker import Checker

from cerbere.feature.profile import Profile


class CMEMSInsituDatasetChecker(Checker, unittest.TestCase):
    """Test class for GCMEMSInsituDataset files"""

    def __init__(self, methodName="runTest"):
        super(CMEMSInsituDatasetChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the mapper class name"""
        return 'CMEMSInsituDataset'

    @classmethod
    def feature(cls):
        """Return the related datamodel class name"""
        return 'IMDCollection'

    def _create_feature(self, *args, **kwargs):
        return super(CMEMSInsituDatasetChecker, self)._create_feature(
            *args, feature_class=Profile, **kwargs
        )

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "GL_PR_PF_5904121_20200317.nc"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
