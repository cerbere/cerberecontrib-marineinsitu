"""
Test class for cerbere CMEMSInsituDataset dataset

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import os

from cerbere.feature.pytest_cfeature import *
from cerbere.feature.cimdcollection import IMDCollection
from cerbere.feature.cprofile import Profile


TEST_FILE = Path(
    os.getenv('CERBERE_TESTDATA_REPOSITORY'),
    'CMEMSINSTAC',
    'CO_DMQCGL01_20201222_PR_PF.nc',
)


@pytest.fixture(scope='module')
def test_file():
    return TEST_FILE


@pytest.fixture(scope='module')
def test_var():
    return 'TEMP'


@pytest.fixture(scope='module')
def feature_instance():
    xrdataset = cerbere.open_dataset(TEST_FILE)
    return IMDCollection(xrdataset, instance_class='Profile')


@pytest.fixture(scope='module')
def reader():
    return 'CMEMSINSTAC'


@pytest.fixture(scope='module')
def feature_class():
    return IMDCollection


@pytest.fixture(scope='module')
def instance_class():
    return Profile


@pytest.fixture(scope='module')
def feature_dims():
    return {'N_PROF': 694, 'N_LEVELS': 1393}
