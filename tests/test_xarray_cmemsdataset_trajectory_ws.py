"""
Test class for cerbere CMEMSInsituDataset dataset

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import os
from pathlib import Path

import pytest

import cerbere
from cerbere.reader.pytest_reader import *
from cerbere.feature.ctrajectory import Trajectory


TEST_FILE = Path(
    os.getenv('CERBERE_TESTDATA_REPOSITORY'),
    'CMEMSINSTAC',
    'GL_WS_MO_41010_20201006.nc',
)


@pytest.fixture(scope='module')
def test_file():
    return TEST_FILE


@pytest.fixture(scope='module')
def reader():
    return 'CMEMSINSTAC'


@pytest.fixture(scope='module')
def feature_class():
    return Trajectory


def test_guess_backend(test_file):
    """Open file and create dataset object"""
    dst = cerbere.open_dataset(test_file).cb.cfdataset
    assert 'time' in dst
    assert 'lat' in dst
    assert 'lon' in dst
