"""
Test class for cerbere CMEMSINSTAC dataset

:copyright: Copyright 2020 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import os
from pathlib import Path

import pytest

import cerbere
from cerbere.reader.pytest_reader import *
from cerbere.feature.ctrajectoryprofile import TrajectoryProfile


TEST_FILE = Path(
    os.getenv('CERBERE_TESTDATA_REPOSITORY'),
    'CMEMSINSTAC',
    'GL_PR_PF_5904121_20200317.nc',
)


@pytest.fixture(scope='module')
def test_file():
    return TEST_FILE


@pytest.fixture(scope='module')
def reader():
    return 'OSISAFSCATL2'


@pytest.fixture(scope='module')
def feature_class():
    return TrajectoryProfile


def test_guess_backend(test_file):
    """Open file and create dataset object"""
    dst = cerbere.open_dataset(test_file).cb.cfdataset
    assert 'time' in dst
    assert 'lat' in dst
    assert 'lon' in dst

#
# def testIdentifier(self):
#     datasetobj = self.datasetclass(self.testfile)
#     feature = self._create_feature(datasetobj)
#
#     import numpy as np
#     from cerbere.dataset.field import Field
#
#     feature.add_field(Field(
#         name='platform_id',
#         data=np.ma.array([str(feature.attrs['wmo_platform_code']).strip()]
#                          * feature.dims['profile']),
#         dtype='O',
#         dims=feature.get_coord('lat').dims,
#         attrs={
#             'long_name': (
#                 'WMO identifier of the measuring platform (buoy, ship,...)')
#         }
#     ))
#
#     # subset
#     dst = feature.extract(index={'z': slice(0, 5)}, padding=True)
#     print(dst)
